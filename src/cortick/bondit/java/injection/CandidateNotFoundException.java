package cortick.bondit.java.injection;

/**
 * Created by bondit on 17.10.15.
 */
public class CandidateNotFoundException extends RuntimeException {
    public CandidateNotFoundException(String message) {
        super(message);
    }
}
