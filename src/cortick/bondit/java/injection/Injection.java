package cortick.bondit.java.injection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by bondit on 16.10.15.
 */
public class Injection {

    public static final Object EMPTY_OBJECT = null;
    private static Map<String, Object> graph = new ConcurrentHashMap<>();

    private static BeanConfiguration configuration;
    private static List<Method> declaredMethods;
    private static Set<String> androidInjectedClasses;

    public static void configure(BeanConfiguration beanConfiguration) {
        configuration = beanConfiguration;
        setDeclaredMethods();
        createBeans();
    }

    private static void createBeans() {
        createBeans(null);
    }

    private static void createBeans(String beanName) {
//        if (beanName != null) {
//            if (androidInjectedClasses.contains(beanName)) {
//                graph.put(beanName, EMPTY_OBJECT);
//            }
//        }
        for (Method declaredMethod : declaredMethods) {
            if (beanName == null || declaredMethod.getName().equals(beanName)) {
                addBean(declaredMethod);
            }
        }
    }

    private static void addBean(Method declaredMethod) {
        try {
            Bean beanAnnotation = declaredMethod.getAnnotation(Bean.class);
            Object object = null;
            if (!beanAnnotation.lazy()) {
                if (graph.containsKey(declaredMethod.getName())) {
                    Logger.getGlobal().log(Level.INFO, "bean " + declaredMethod.getName() + " already exist");
                    return;
                }

                object = declaredMethod.invoke(configuration);
            }
            graph.put(declaredMethod.getName(), object);
//            Log.i(TAG, declaredMethod.getName());

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private static void setDeclaredMethods() {
        declaredMethods = new ArrayList<>();
        androidInjectedClasses = new HashSet<>();

        Class<? extends BeanConfiguration> clazz = configuration.getClass();

        for (Method method : clazz.getDeclaredMethods()) {
            Bean annotation = method.getAnnotation(Bean.class);
            if (annotation != null) {
                if (annotation.lazy()) {
                    androidInjectedClasses.add(getBeanName(method.getReturnType().getSimpleName()));
                } else {
                    declaredMethods.add(method);
                }
            }
        }
    }

    private static String getBeanName(String simpleName) {
        return simpleName.substring(0, 1).toLowerCase() + simpleName.substring(1);
    }

    public static void inject(Object injectedObject) {
        Logger.getGlobal().log(Level.INFO, "inject into " + injectedObject);
//        Log.i(TAG, "inject into " + injectedObject);
        mapInjectSelfIfNeed(injectedObject);
        mapAutowiredFields(injectedObject);
//        printGraph();
    }

//    private static void printGraph() {
//        for (String s : graph.keySet()) {
//            Log.i(TAG, s);
//        }
//    }

    private static void mapAutowiredFields(Object injectedObject) {
        mapAutowiredFields(injectedObject, false);
    }

    private static void mapAutowiredFields(Object injectedObject, boolean override) {
        Field[] fields = getAutowiredFields(injectedObject);

        for (Field field : fields) {
            try {
                Object o = field.get(injectedObject);
                if (o == null || override) {
                    Autowired autowiringField = field.getAnnotation(Autowired.class);
                    String fieldName = provideFieldName(field.getName(), autowiringField.qualifier());
                    Object candidate = findCandidate(fieldName);
                    if (candidate == null || override) {
                        Logger.getGlobal().log(Level.INFO, "creating candidate: " + fieldName);
//                    Log.i(TAG, "creating candidate: " + fieldName);
                        candidate = createCandidate(fieldName);
                    }
                    if (candidate != null) {
                        field.set(injectedObject, candidate);
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static Field[] getAutowiredFields(Object injectedObject) {
        Map<String, Field> autowiredFields = new HashMap<>();

        Class clazz = injectedObject.getClass();
        while (clazz != Object.class) {
            for (Field field : clazz.getDeclaredFields()) {
                Autowired annotatedField = field.getAnnotation(Autowired.class);
                if (annotatedField != null) {
                    autowiredFields.put(field.getName(), field);
                }
            }
            clazz = clazz.getSuperclass();
        }
        return autowiredFields.values().toArray(new Field[]{});
    }

    private static void mapInjectSelfIfNeed(Object injectedObject) {
        String simpleClassName = getBeanName(injectedObject.getClass().getSimpleName());
        if (androidInjectedClasses.contains(simpleClassName)) {
//            if (!graph.containsKey(simpleClassName)) {
                graph.put(simpleClassName, injectedObject);
                injectIntoExistingObjects(simpleClassName, injectedObject);
//            }
        }
    }

    private static void injectIntoExistingObjects(String simpleClassName, Object injectedObject) {
        for (Object o : graph.values()) {
            if (o != null) {
                mapAutowiredFields(o, true);
            }
        }
    }

    private static Object createCandidate(String fieldName) {
        createBeans(fieldName);
        return findCandidate(fieldName);
    }

    private static Object findCandidate(String fieldName) {
        Object candidate = graph.get(fieldName);
        return candidate;
    }

    private static String provideFieldName(final String originFieldName, final String qualifier) {
        String fieldName = qualifier;
        if (fieldName.equals("")) {
            fieldName = originFieldName;
        }
        return fieldName;
    }
}
